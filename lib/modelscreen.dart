import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'Model/model.dart';
import 'articlescreen.dart';
import 'dart:async';

Future<List<Source>> fetchNewsSource() async {
  final response = await http.get(
      'https://newsapi.org/v2/sources?apiKey=6312aeb509fc42628b215096215d9619');
  if (response.statusCode == 200) {
    List sources = json.decode(response.body)['sources'];
    return sources.map((source) => new Source.fromJson(source)).toList();
  } else {
    throw Exception('Failed to load');
  }
}

void main() => runApp(new SourceScreen());

class SourceScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SourceScreenState();
}

class SourceScreenState extends State<SourceScreen> {
  var list_sources;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    refreshListSource();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold( 
        body: Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Center(
        child: RefreshIndicator(
            key: refreshKey,
            child: FutureBuilder<List<Source>>( // Future builder
              future: list_sources,
              builder: (context, snapshot) {if (snapshot.hasData) {
                  List<Source> sources = snapshot.data;
                  return new ListView(
                      children: sources
                          .map((source) => GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ArticleScreen(
                                                source: source,
                                    )));
                                },
                                child: Card(
                                  elevation: 2.0,
                                  color: Colors.white,
                                  margin: const EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 3),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 2.0),
                                        width: 100,
                                        height: 120,
                                        child: Image.network("https://lh3.googleusercontent.com/ALrXQhTm_DAJCkShULK6_a4HdDOk_WiIXn1-Vkidm5ET2bcv-duVn9XfSHPXCTEnFxRypK7qKp-ifgaOCE3vdhUk=s1024"),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Container(
                                                    margin:
                                                        const EdgeInsetsDirectional
                                                                .only(
                                                            top: 20,
                                                            bottom: 10),
                                                    child: Text(
                                                      '${source.name}',
                                                      style: TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                            Container(
                                              child: Text(
                                                '${source.description}',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                maxLines: 3,
                                              ),
                                            ),
                                            Container(
                                              child: Text(
                                                'Category:  ${source.category}',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ))
                          .toList());
                }
                return CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.teal),
                  backgroundColor: Colors.grey[300],
                  strokeWidth: 2.0,
                );
              },
            ),
            onRefresh: refreshListSource),
      ),
    ));
  }

  Future<Null> refreshListSource() async {
    refreshKey.currentState?.show(atTop: false);
    setState(() {
      list_sources = fetchNewsSource();
    });
    return null;
  }
}
