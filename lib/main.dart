import 'package:flutter/material.dart';
import 'homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        title: "News App",
        home: HomePage());
  }
}

class MyAppHome extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new _MyAppHomeState();
}


class _MyAppHomeState extends State<MyAppHome> {

  @override
  Widget build(BuildContext context) {
  }
}
