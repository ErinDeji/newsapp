import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

import 'Model/model.dart';

Future<List<Article>> fetchArticleBySource(String source) async {
  final response = await http.get(
      'https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=6312aeb509fc42628b215096215d9619');
  if (response.statusCode == 200) {
    List articles = json.decode(response.body)['articles'];
    return articles.map((article) => new Article.fromJson(article)).toList();
  } else {
    throw Exception('Failed to load source list');
  }
}

class ArticleScreen extends StatefulWidget {
  final Source source;

  ArticleScreen({Key key, @required this.source}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ArticleScreenState();
}

class ArticleScreenState extends State<ArticleScreen> {
  var list_articles;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    refreshListArticle();
  }

  Future<Null> refreshListArticle() async {
    refreshKey.currentState?.show(atTop: false);
    setState(() {
      list_articles = fetchArticleBySource(widget.source.id);
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'NEWS APP',
        theme: ThemeData(primarySwatch: Colors.teal),
        home: Scaffold(
          appBar: AppBar(
            title: Text(widget.source.name),
          ),
          body: Center(
            child: RefreshIndicator(
                key: refreshKey,
                child: FutureBuilder<List<Article>>( // implemented FutureBuilder
                  future: list_articles,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('${snapshot.error}');
                    } else if (snapshot.hasData) {
                      List<Article> articles = snapshot.data;
                      return ListView(
                        children: articles
                            .map((article) => GestureDetector(
                                  onTap: () {
                                    _launchUrl(article.url);
                                  },
                                  child: Card(
                                    elevation: 2.0,
                                    color: Colors.white,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 2, horizontal: 8),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.symmetric(
                                              vertical: 7, horizontal: 4),
                                          width: 100,
                                          height: 100,
                                          child: article.urlToImage != null
                                              ? Image.network(
                                                  article.urlToImage)
                                              : Image.asset('assets/oggz.jpg'),
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                        margin: const EdgeInsets
                                                                .only(
                                                            left: 8,
                                                            top: 20,
                                                            bottom: 10),
                                                        child: Text(
                                                          '${article.title}',
                                                          style: TextStyle(
                                                              fontSize: 17,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        )),
                                                  )
                                                ],
                                              ),
                                              Container(
                                                margin: const EdgeInsets.only(
                                                    left: 8),
                                                child: Text(
                                                  '${article.description}',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                              Container(
                                                margin: const EdgeInsets.only(
                                                    left: 8,
                                                    top: 10,
                                                    bottom: 10),
                                                child: Text(
                                                  'Date Uploaded:  ${article.publishedAt}',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                          
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ))
                            .toList(),
                      );
                    }
                    return CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.teal),
                      backgroundColor: Colors.grey[300],
                      strokeWidth: 2.0,
                    );
                  },
                ),
                onRefresh: refreshListArticle),
          ),
        ));
  }

  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw ('Couldn\'t launch ${url} ');
    }
  }
}
