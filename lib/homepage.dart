import 'package:flutter/material.dart';
import 'package:newsapp/modelscreen.dart';

class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    // TODO: implement initState
    return null;
  }

  @override
  Widget build(BuildContext context) {
    // main screen
    return MaterialApp(
      title: 'NEWS APP',
      theme: ThemeData(primarySwatch: Colors.teal),
      home: Scaffold(
          appBar: AppBar(
              title: Text('NEWS APP',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              ),
          body: SourceScreen()),
    );
  }
}